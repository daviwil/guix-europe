title: GNU Guix Days FOSDEM 2020
date: 2022-11-13 0:00
---
This page was for tracking anything related with the 2020 GNU Guix/Guile
conference and hackathon on Jan. 30th and 31st — right before
[FOSDEM2020](https://fosdem.org/2020/) and the [Minimalistic,
Experimental and Emerging Languages
devroom](https://fosdem.org/2020/schedule/track/minimalistic_experimental_and_emerging_languages/)
and the original application [minimalistic language
devroom](https://libreplanet.org/wiki/FOSDEM2020-devroom-proposal).

![Guix Days 2020 logo](/images/Guix-Days-2020.png)

Annual GNU Guix (un)conference. This event was a [FOSDEM 2020
fringe event](https://fosdem.org/2020/fringe/).

## Why?

GNU Guix is growing rapidly and has gone from a software packaging
system to a full tool stack aimed at reproducible software deployment
and development. GNU Guix is a toolkit that allows developers to
integrate reproducible software deployment into their applications — as
opposed to leaving it up to the user. GNU Guix is based on the GNU Guile
programming language which makes it a very versatile and hackable (in
the good sense) environment.

## When?

Thursday 30th and Friday 31st of January, 2020, the two days before
FOSDEM.

Starting at 9:30AM.

## Where?

[Institute of Cultural Affairs (ICAB)](http://icab.be/) Rue Amedee Lynen
8 1210 Brussels
([map](https://www.openstreetmap.org/?mlat=50.85019&mlon=4.37320#map=18/50.85019/4.37320)),
Belgium.

## Programme

We had some talks in the morning and hacking and discussions the
rest of the day. Two days of GNU Guix bliss.

The event targeted an audience of Guix developers and users.

In the morning the following presentations were planned:

1. **Guix State of the Union** by Ludovic Courtès + Tobias
    Geerinckx-Rice
2. **Guix Data Service** by Christopher Baines
3. **Bootstrapping Intro** by Jan Nieuwenhuizen

In pure "unconference" style, the rest of the program was made by
participants as we went, with hands-on sessions organised in subgroups.
The following session topics were proposed:

1. GNU Guix road map
2. GNU Guix online documentation
3. Mes and bootstrapping
4. Reproducibility project
5. Alternative target architectures (ARM, POWER etc.)
6. Developing workflows (GNU Guix and GNU Workflow Language)
7. Support for D in GNU Guix
8. Demonstration and explanation of [The Perfect
    Setup](https://www.gnu.org/software/guix/manual/en/guix.html#The-Perfect-Setup)
    (Emacs, Geiser, Magit)
9. Demonstration and live-hacking of [Next
    browser](https://next.atlas.engineer) + introduction to the code
    base
10. Better build systems for JVM languages in GNU Guix (support Maven,
    sbt, Gradle, \...)
11. Reproducible JDK
12. The Guix System installer
13. Improve PHP
14. your topic?

## Code of conduct

Attendees implicitly abode by the [code of conduct as
stated by FOSDEM](https://fosdem.org/2020/practical/conduct/).

## Participants

1. Ludovic Courtès
2. Pjotr Prins
3. Manolis Ragkousis
4. Efraim Flashner
5. Tobias Geeri**nckx**-Rice (9 to 5, ish)
6. Christopher Baines
7. Gábor Boskovits
8. Andreas Enge
9. Simon Tournier
10. Nicolò Balzarotti
11. Chris Marusich
12. Jonathan Brielmaier
13. Danny Milosavljevic
14. Carl Dong
15. Pierre Neidhardt
16. janneke (Jan) Nieuwenhuizen
17. Martin Becze
18. Mark Wielaard (Thursday afternoon and Friday)
19. Tom Tromey
20. Bengt Richter
21. Ioanna Dimitriou
22. Joshua Wirtz
23. Jelle Licht
24. Mathieu Othacehe (only Friday)
25. Jonathan Frederickson
26. Lex Withofs
27. Laura Lazzati
28. Paul Garlick (Friday)
29. Jonathan McHugh
30. Lars-Dominik Braun
31. Pablo Greco (only Thursday)
32. Robert Smith
33. Jérémy Korwin (only Friday)
34. Mayeul de Bellabre
35. Ivan Vilata i Balaguer (only Friday)
36. Tobias Platen (only Friday)

## Costs

Attendance was free. We asked a voluntary contribution for
consumptions.

## Dinner

- Thursday: [Xu-Ji](https://www.openstreetmap.org/way/524013968)
- Friday:
    [Santorini](https://www.takeaway.com/be/santorini-bruxelles),
    [map](https://www.openstreetmap.org/?mlat=50.84577&mlon=4.35262#map=19/50.84577/4.35262))

## Sponsor

- Foundation of Guix Europe

