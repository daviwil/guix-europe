title: Minutes
date: 2023-07-14 0:00
---
## General Assemblies

The General Assembly usually takes place once a year by videoconference;
its minutes are public.

* [2023-06-10](/minutes/ga-20230610.txt)
* [2023-05-26](/minutes/ga-20230526.txt)
* [2022-07-17](/minutes/ga-20220717.txt)
* [2022-07-03](/minutes/ga-20220703.txt)
* [2021](/minutes/ga-20211106.txt)
* [2020](/minutes/ga-20200621.txt)
* [2017](/minutes/ga-20171219.txt)


## Solidary Administrative Council

The SAC is the main decision taking body of Guix Europe. It is
currently composed of
* Adriel Dumas--Jondeau
* Bertrand Mathelier
* Andreas Enge
* Christopher Baines
* David Wilson
* Efraim Flashner
* Jelle Licht
* Jonathan Brielmaier
* Julien Lepiller (Treasury)
* Manolis Ragkousis
* Oliver Propst
* Pjotr Prins
* Ricardo Wurmus
* Simon Tournier (Presidency)
* Tanguy Lecarrour
* Timo Wilken


The SAC minutes are public:
* 2023
  * [06-13](/minutes/sac-20230613.txt)
  * [05-25](/minutes/sac-20230525.txt)
  * [02-18](/minutes/sac-20230218.txt)
* 2022
  * [09-28](/minutes/sac-20220928.txt)
  * [09-23](/minutes/sac-20220923.txt)
  * [09-18](/minutes/sac-20220918.txt)
  * [06-25](/minutes/sac-20220625.txt)
  * [06-23](/minutes/sac-20220623.txt)
* 2020
  * [06-12](/minutes/sac-20200612.txt)
  * [04-25](/minutes/sac-20200425.txt)
  * [01-28](/minutes/sac-20200128.txt)
  * [01-25](/minutes/sac-20200125.txt)
* 2019
  * [06-06](/minutes/sac-20190606.txt)
  * [06-03](/minutes/sac-20190603.txt)
  * [02-10](/minutes/sac-20190210.txt)
  * [01-28](/minutes/sac-20190128.txt)
  * [01-23](/minutes/sac-20190123.txt)
* 2018
  * [03-08](/minutes/sac-20180308.txt)
* 2017
  * [12-18](/minutes/sac-20171218.txt)
  * [11-16](/minutes/sac-20171116.txt)
  * [02-21](/minutes/sac-20170221.txt)
* 2016
  * [11-11](/minutes/sac-20161111.txt)
  * [06-02](/minutes/sac-20160602.txt)
  * [04-17](/minutes/sac-20160417.txt)
  * [03-10](/minutes/sac-20160310.txt)

