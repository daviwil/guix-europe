Minutes of the Solidary Administrative Council meeting of 2020-04-25


Present: Christopher Baines, Ludovic Courtès, Andreas Enge, Oliver Propst,
Ricardo Wurmus


1) Membership requests

The membership requests by Jonathan Brielmaier and Tobias Geerinckx-Rice
are accepted unanimously.

