Minutes of the ordinary General Assembly of 2020-06-21

Present during the online meeting on bigbluebutton:
Andreas Enge, Christopher Baines, Efraim Flashner, Julien Lepiller,
Jonathan Brielmaier, Ludovic Courtès, Manolis Ragkousis,
Oliver Propst (only in the beginning), Pjotr Prins, Ricardo Wurmus,
Simon Tournier;
11 out of 19 registered members, the quorum of 2/3 is not reached


1) Activity report by the Presidency

   Manolis presents the following activities of Guix since the last
   General Assembly in December 2017.

   2018
   * In November-December of 2017 we started organising the Guix days,
     in response to not getting a Guile devroom at FOSDEM.
   * In February 2018 we successfully held a one day GNU Guix/Guile day
     event, where we had presentations, discussions and lots of hacking.
     We had 32+ participants.
   * In September we decided to do a Minimalistic Languages Devroom at
     FOSDEM 2019, including any language that fits the term "minimalistic".
     Our application was successful, and we got a whole day devroom.
   * From September to November we reached out to people from different
     projects and invited them to give a talk in the devroom.
   * In December we started organising Guix Days for 2019-01-31 to 02-01.
     This time we booked the whole ICAB as there was a lot of interest last
     year.
   * In December we finalised the FOSDEM talk programme.

   2019
   * We held Guix Days in ICAB, Brussels. Participants 35+, great success.
   * We held the Minimalistic Languages Devroom at FOSDEM with great success.
   * In September we were again accepted as a full day room, but were
     merged with Experimental and Emerging languages. The Minimalistic,
     Experimental and Emerging Languages devroom was born. This is how we
     intend to continue next year as well.
   * Again, from September to November we reached out to people from
     different projects and invited them to give a talk in the devroom.
   * In December we started organizing Guix Days from 2020-01-30 to 31.
   * In December we finalised the FOSDEM talk program

   2020
   * We held Guix Days in ICAB, Brussels. Participants 36+, even greater
     success than the previous year.
   * We held the Minimalistic, Experimental and Emerging Languages devroom
     with even greater success.


2) Financial report by the Treasury
   
   Andreas has sent out the following report by e-mail before the assembly;
   there are no further questions.

   As a general remark, our finances cannot be considered separately from
   those held for the Guix project at the FSF, following our fundraiser
   quite some time ago. In many cases, we just advance money that in the
   end will be reimbursed by the FSF. Our own funds come from a big initial
   donation of 5000€ by Igalia, specific donations for holding the Guix Days
   (see below) and membership fees. Our accounts are publicly available in
   the "maintenance" git repo at
      https://git.savannah.gnu.org/cgit/guix/maintenance.git/tree/guix-europe/accounting ;
   in particular the ledger file is
      https://git.savannah.gnu.org/cgit/guix/maintenance.git/tree/guix-europe/accounting/accounting.ledger

   Let us jump in, with the output of
   $ guix environment --ad-hoc ledger -- ledger -f accounting.ledger balance

             €708.84  Assets
             €702.84    Bank
               €6.00    Stock
               €1.00      Bayfront
               €2.00      Overdrive 1000
               €3.00      Redhill
              €-6.00  Equity
           €12771.85  Expenses
              €15.00    Banking
             €190.88    Domains
            €4987.70    Guix Day
            €3275.00    Hosting
              €44.00    Legal
              €80.00    Membership
              €60.00    Promotional material
            €4119.27    Purchases
          €-13474.69  Income
           €-5690.00    Donations
           €-2726.00    Guix Day
           €-2675.00    Hosting
              €-0.87    Interests
            €-590.00    Membership
             €-80.00      2016
            €-110.00      2017
            €-120.00      2018
            €-170.00      2019
            €-110.00      2020
           €-1792.82    Purchases
          ----------
                   0

   In the last few lines, you see a breakdown of our membership fees, that
   have been growing with a slow, but steady increase of members. On the
   other hand, membership at Aquilenet, where the bayfront machine is
   hosted, costs us 20€ per year.

   Hosting bayfront is 75€ a month, corresponding to the cost incurred by
   the non-profit Aquilenet association; it is entirely reimbursed by the
   FSF (currently up to and including October 2019).

   Expenses for "domains" correspond to the domains guixsd.org (that will
   run out next year) and guix.info (just renewed for five years).

   "Banking" are fees due to a shared-cost wire transfer by the FSF, whereas
   our bank account at Banque Populaire Centre Atlantique is free of charge.

   "Promotional material" are Guix stickers.

   "Purchases" correspond to expenses for bayfront and redhill from 2016 to
   2018 (taken from our funds), as well as three Overdrive buld servers
   (reimbursed by the FSF, which is why they also appear as "income").

   Maybe the most interesting entries correspond to the Guix Days, that we
   have held since 2018 prior to FOSDEM in Brussels. Expenses are the rent
   at ICAB, including the provided lunch and coffee breaks. Income are lunch
   collection (we ask for a voluntary cost-covering participation) and
   donations: In 2018, the meetings were jump-started by 200€ by Pjotr's
   company GeneNetwork; in 2019, GeneNetwork and Janneke's company Joy of
   Source both helped us with 200€ for expanding the meeting to two days.
   Many thanks again! Here is a more detailed breakdown:

   Expenses 2018
   Rent        749.70
   Lunch      -210
   Donations  -200
   The remaining 339.70 were borne by Guix Europe.

   Expenses 2019
   Rent       1796    (for two days instead of one, and two rooms)
   Lunch      -693.50
   Donations  -400.00
   Of the remaining 702.50, 502.50 were taken out of our funds at the FSF
   and 200 were paid by Guix Europe.

   Expenses 2020
   Rent       2442    (we were more people, so catering was more expensive,
                       and we also took more generous coffee breaks)
   Lunch      -720
   For the remaining 1722€, again 200€ will be paid by Guix Europe, and
   1522€ will be taken out of the funds at the FSF.

   In 2019, we had an exceptional donation of 680€ by Fund the Code,
   following a presentation of Guix by Mathieu and Clément.


   Our current (at the end of June) balance is of 702.84€ in our bank
   account. As explained above, we will ask for a reimbursement of 1522€
   for the Guix Days 2020, and of 600€ (8*75€) for bayfront hosting from
   November 2019 to June 2020.

   So altogether, the association owns 2824.84€, which can be used as a
   buffer or liquidity reserve for expenses that will be reimbursed by the
   Guix funds at the FSF, or for additional projects.


3) Election of the new Presidency and Treasury

   Manolis represents himself for Presidency. Andreas represents himself
   for Treasury, but points out that he would like to pass on this
   responsability in the long run. During the discussion it is suggested
   to create the role of Treasury aide, who could become Treasury in the
   future; Chris volunteers for this role.

   All three are elected unanimously.


4) Election of the Solidary Administrative Council (SAC)

   All current members of the SAC are present, and all present are
   interested in joining the SAC. They are elected unanimously. So
   the current SAC is formed by:
   Andreas Enge, Christopher Baines, Efraim Flashner, Julien Lepiller,
   Jonathan Brielmaier, Ludovic Courtès, Manolis Ragkousis,
   Oliver Propst, Pjotr Prins, Ricardo Wurmus, Simon Tournier.


5) Vote on the changes to the Statutes and Interior Reglementary as
   suggested by Simon Tournier

   The changes concern only typos, but we must still follow our statutes,
   which require the presence of 2/3 of the members, which is not reached.
   So this topic cannot be voted on; we decide instead to call an
   Extraordinary General Assembly and to allow a vote by email at this
   occasion.


6) Future activities

   We discuss whether to add a third day to the Guix Days, given their
   huge success. But counting FOSDEM, altogether it is alrady a four-day
   event. Some participants already come for only one of the two days.
   So the consensus is to keep the Guix Days duration in 2021.

   The question is whether ICAB will be big enough given our gradual
   increase in the number of participants. For 2021, there is consensus
   that we should book ICAB again, and reevaluate the situation if and
   when the need arises.

   In addition to the Guix Days, we consider holding a purely virtual event
   towards the end of the year; during the discussion, September or the time
   around Christmas are mentioned, without clear consensus. Manolis, Pjotr
   and Jonathan volunteer to work on the organisation.

   In case FOSDEM is cancelled due to ongoing Covid problems, we would also
   like to hold the Guix Days as an online event. When booking ICAB, we
   need to watch out for a suitable cancellation policy.


7) Travel grants

   It is suggested to provide travelling support for one or two students
   who work on Guix to attend the Guix Days; it would not need to cover
   all of the costs. While it appears that GSoC and Outreachy Students have
   a travel grant as part of their internship, this might be interesting
   to support otherwise non-funded persons and in particular members of
   underrepresented groups. There appears to be consensus about the
   usefulness of such an effort, but given the Guix Europe funds, this
   would have to be paid out of the funds held at the FSF, and thus
   requires discussion with the Guix spending committee.


8) Fundraising

   The previous topic leads to the question whether we should start another
   fundraiser. Given our funds at the FSF, this will probably not be needed
   before another year. Then we should determine a concrete goal that should
   be reached to motivate donations, and start a joint fundraiser with the
   FSF and Guix Europe.

