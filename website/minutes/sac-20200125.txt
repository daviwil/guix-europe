Minutes of the Solidary Administrative Council meeting of 2019-01-25


Present: Ludovic Courtès, Andreas Enge, Ricardo Wurmus, Oliver Probst


1) Domain name guixsd.org

The following is decided unanimously:
- We renew the domain again for one year.
- We do not renew the domain again after that year.

