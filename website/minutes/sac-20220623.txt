Minutes of the Solidary Administrative Council meeting of 2022-06-23


Present: Simon Tournier, Efraim Flashner, Julien Lepiller, Andreas Enge


1) 10 years of Guix event, sponsoring

It is decided unanimously to sponsor the event for the 10th anniversary
of Guix, to be held from September 16 to 18 in Paris, with 200€.
It is also decided unanimously to advance expenses required for this event,
within the limits of promised reimbursements by other entities. The Guix
Spending Committee, for instance, has agreed to a reimbursement of up to
3000€ by the funds held at the FSF.


2) 10 years of Guix event, publicity

It is decided unanimously to use the 10th anniversary event to issue a
call for membership.

